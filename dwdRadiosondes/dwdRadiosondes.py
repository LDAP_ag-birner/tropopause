from io import StringIO, BytesIO
from zipfile import ZipFile
from urllib.request import urlopen
from urllib.error import HTTPError
import pandas as pd
import requests
from bs4 import BeautifulSoup
import urllib.request
from datetime import datetime


def radiosonde_df(station_id, years=None):
    """
    parse dwd open data archive and download radiosond data for a given station and a given list of years
    return: dataframe
    """
    station_id = str(station_id).zfill(5)
    years = years if years else list(range(1947, 2020))
    years = [years] if not isinstance(years, list) else years
    df_list = []

    for yr in years:
        url_oneyr = "https://opendata.dwd.de/climate_environment/CDC/observations_germany/radiosondes/low_resolution/historical/{year}/".format(
            year=yr
        )

        # list all zip links in the year folder
        files_in_oneyr_dir = listFD(url_oneyr)

        # find the url for the zip file that matches the station_id
        url = None
        for potential_file in files_in_oneyr_dir:
            if (
                "punktwerte_aero_{station_id}".format(station_id=station_id)
                in potential_file
            ):
                url = url_oneyr + potential_file
                break

        if url:
            try:
                # open url
                # to pandas dataframe
                df_oneyr = urlzip_to_df(url)
                df_list.append(df_oneyr)
            except HTTPError:
                print("Page not found (year = {})".format(yr))

    # combine dataframes from different years
    df = pd.concat(df_list)

    # rename columns to physical quantity
    df = radiosonde_rename_cols(df)

    # if there are duplicates on time and pressure, use the column with maximum temperature
    df = df.groupby(["MESS_DATUM", "p"]).max(numeric_only=True).reset_index("p")

    # sort again by time and descending pressure
    df = df.sort_values(["MESS_DATUM", "p"], ascending=[True, False])

    # drop NaN
    df = df[df["t"].notna()]
    
    # introduce measurement index
    df = radiosonde_add_measurement_index(df)

    # convert hPa to Pa
    df["p"] *= 100
    
    # ensure 5 digits for station_id
    df["station_id"].apply(lambda x: str(x).zfill(5))

    # rename MESS_DATUM to time
    df = df.rename_axis(index={"MESS_DATUM": "time"})
    return df


def urlzip_to_df(url):
    """
    convert an url of a zip file directly to a pandas dataframe
    return: dataframe
    """
    resp = urlopen(url)
    zipfile = ZipFile(BytesIO(resp.read()))
    file = zipfile.namelist()[0]

    lines = []
    for line in zipfile.open(file).readlines():
        lines.append(line.decode("utf-8").strip())
    lines_str = StringIO("\n".join(lines))

    df = pd.read_csv(
        lines_str,
        sep=";",
        parse_dates=["MESS_DATUM"],
        date_parser=lambda x: datetime.strptime(x, "%Y%m%d%H"),
        index_col=["MESS_DATUM"],
        na_values=[-999.0, -999],
    )
    return df


def listFD(url):
    """
    check for all links on url page
    return: list of strings
    """
    page = requests.get(url).text
    soup = BeautifulSoup(page, "html.parser")
    links = [
        link["href"] for link in soup.findAll("a", href=True) if (link["href"] != "../")
    ]
    return links


def radiosonde_rename_cols(df):
    """
    rename a dataframe that follows a standard dwd radiosonde file
    return: dataframe
    """
    names = [
        "station_id",
        #    "time",
        "qn_1",
        "p",
        "height",
        "ind_bo",
        "ind_inter",
        "ind_maxw",
        "ind_mpt",
        "int_mpw",
        "ind_trop",
        "t",
        "rel_humidity",
        "dew_point",
        "wind_speed",
        "wind_dir",
        "eor",
    ]
    renaming = dict(zip(df.columns.values, names))

    return df.rename(columns=renaming)


def radiosonde_add_measurement_index(df, groupby_col="MESS_DATUM", name="mi"):
    # introduce measurement index
    df[name] = df.groupby(groupby_col).transform(lambda x: range(len(x))).iloc[:, 0]
    return df.set_index(name, append=True)