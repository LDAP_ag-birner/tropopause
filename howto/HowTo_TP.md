# Documentation for Tropopause Calculation Function

This describes the process of calculating the tropopause based on the WMO's definition from 1992. The tropopause is:

1. the lowest layer at which the lapse rate decreases to 2 K/km or less and
2. where all average lapse rates between this level and all higher levels within 2 km do not exceed 2 K/km

The first criterion is easily checked by calculating the zero crossings of the vertical derivative of the temperature shifted by 2 K/km.

The second criterion (also called thickness criterion) can be checked by iteratively calculating the average lapse rate between the level with the preliminary tropopause and all the levels above within 2 kilometers (first the average between the tropopause level and the one above, then the average). A simple function to check this could look like this (pseudo code):

```
tropopause_criterion(dtdz, dz, gamma_crit = -2.0):
    for levels in 2 km:
        if average(dtdz[levels] < gamma_crit):
            return False

    return True
```

Algorithmically the process involves a few more calculations but they wrap around checking these two criteria for every potential tropopause (criterion 1), going through them upwards from the lowermost one.

### Exemplary Calculation
#### Lindenberg Station, 09-01-1962, 6 pm


![](example_t-profile_09-01-1962_1800.png)

The radiosonde data provides temperature, pressure and height information. If only pressure and temperature data is available the calculation involves a few more steps to estimate the dz intervals from the pressure data.

Either way the first step is to get the vertical derivative of the temperature with respect to height, dt/dz.

The first criterion (drop below 2 K/km) is then simple to check by calculating zero crossings in the vertical derivative of temperature, shifted by the threshold:

$zero\_crossing(\frac{dt}{dz} - (-2.0 \frac{K}{km}))$

Only the zero crossings towards a more stable stratification can be potential tropopauses so I filter them by checking the sign of $\frac{dt}{dz} - (-2.0 \frac{K}{km})$ at the level above the zero crossing. In my code I store the indices of the levels of these *preliminary tropopauses* in a list k. The preliminary tropopauses themselves are calculated by linear interpolation in the height profile to the point where dt/dz falls below 2 K/km.


Then for each preliminary tropopause, starting from the lowermost one, I:
- calculate the height interval from the preliminary tropopause to the next level on top of it
- set up an array containing all height intervals in the 2 km above the preliminary tropopause. In most of the cases the last interval needs to be cut to get to 2 km exactly
- set up an array containing all dt/dz values in the 2 km above the tropopause where the first interval is set to 2 K/km.
- check the thickness criterion by the function described above.


The following plots ilustrate the process of iterating through the preliminary tropopauses. The left panels show a portion of the temperature profile, the middle panels show a portion of the vertical derivative, and the right panels show the average derivative from the preliminary tropopauses upwards. Only the preliminary tropopauses from 2500m height upwards are shown here:

The first preliminary tropopauses are very clearly unstable and triggered by small stable layers (<10m).

![](prel_tp_no_1.png)

![](prel_tp_no_2.png)

This preliminary tropopause is triggered by a larger layer with a lapse rate of exaclty 2.0 K/km leading the preliminary tropopause to lie at a pressure level without interpolation. After this the profile is more unstable again --> no tropopause.

![](prel_tp_no_3.png)

This is again triggered by the derivative crossing the threshold and immediately going back. As the average dT/dz drops below -2.0 K/km this is not the tropopause.

![](prel_tp_no_4.png)

The average dT/dz stays above the critical lapse rate for the whole two kilometers. Therefore this preliminary tropopause is taken as the tropopause for this profile. Note that the temperature profile has a higher lapse rate in the layer where the tropopause is calculated. This comes from the interpolation to the point where the lapse rate crosses the critical lapse rate.

![](prel_tp_no_5.png)

#### Lindenberg Station, 31-12-2020, 6 pm

As a second example, the last profile from 2020 with a higher resolution. This time without setting a minimum height.

![](example_t-profile_31-12-2020_1800.png)

including the focus plots for the preliminary tropopauses:

![](ex2_prel_tp_no_1.png)
![](ex2_prel_tp_no_2.png)
![](ex2_prel_tp_no_3.png)
![](ex2_prel_tp_no_4.png)
![](ex2_prel_tp_no_5.png)
![](ex2_prel_tp_no_6.png)
