import xarray as xr  # data handling
import numpy as np  # computating
import pandas as pd  # open data from a textfile

rgas = 287.04
kappa = rgas/1009.
kappai = 1./kappa
g0 = 9.80665
p00 = 1000.


def apply_tropopause_profile(ds, **kwargs):
    """
    Wrapper around tropopause_profile using xarray's apply_ufunc.
    ds must include:
    - pressure levels: plev
    - temperature: temp
    - (geop.) height: height
    
    Returns: the input dataset with added
    - tropopause temperature: ttp
    - tropopause pressure: ptp
    - tropopause height: ztp
    - a flag indicating how often the thickness criterion (WMO 1957) was invoked 
      to discard a potential tropopause: thickness_flag
    """
    
    if ds.plev[-1] > ds.plev[0]:
        ds = ds.reindex(plev=ds.plev[::-1])
        flipped_p = True
    else:
        flipped_p = False
    
    ttp, ptp, ztp, thickness_flag = xr.apply_ufunc(
        tropopause_profile,
        ds.temp,
        ds.plev,
        ds.height,
        kwargs=kwargs,
        input_core_dims=[['plev'],['plev'],['plev']],
        output_core_dims=[[],[],[],[]],  
        vectorize=True,
        dask='parallelized',
    )
    
    ds['ttp'] = ttp
    ds['ptp'] = ptp
    ds['ztp'] = ztp
    ds['thickness_flag'] = thickness_flag
    
    if flipped_p:
        ds = ds.reindex(plev=ds.plev[::-1])
    
    return ds

def tropopause_profile(t_profile, p_profile, z_profile, dtdz_crit=-2.0, tp_thickness= 2000, zmin=0):
    """
    Function calculates the tropopause based on the WMO's definition.
    It returns the tropopause temperature and pressure where the vertical
    temperature gradient dT/dz climbs above dtdz_crit [K/km] and stays above that
    for at least tp_thickness [m].
    Returns: ttp, ptp, ztp, thickness_flag
    """
    if zmin != 0:
        height_cond = np.where(z_profile>=zmin)
        t_profile = t_profile[height_cond]
        p_profile = p_profile[height_cond]
        z_profile = z_profile[height_cond]

    # calculate gamma = dT/dz for each layer
    dz = z_profile[1:] - z_profile[:-1]
    # units [K/km] but the sign isn't switched, so: dtdz = - lapse rate 
    dtdz =  (t_profile[1:] - t_profile[:-1]) / (dz/1000)
    
    # look for tropopause
    # find layer(s) where gamma crosses gamma = dtdz_crit
    ks_raw = zero_crossings(dtdz-dtdz_crit)[0]
    helper = (dtdz-dtdz_crit)[ks_raw]
    ks = ks_raw[np.where(helper > 0)]
    
    # calculate preliminary tropopause levels for each drop below dtdz_crit
    prel_tp = [lin_interp(z_profile[k-1:k+1], dtdz[k-1:k+1], dtdz_crit) for k in ks]
    
    # Set a flag how often the thickness criterion was
    # checked and not fulfilled in the Process
    thickness_flag = 0
    
    # Check if preliminary tropopause fulfills thickness criterion
    for i,ztp in enumerate(prel_tp):
        k = ks[i]
        dz_preltp_up = z_profile[k] - ztp
        # Calculate number of levels inside the tp_thickness (usually the 2km range imposed by the WMO's criterion)
        dz_array = np.append(dz_preltp_up, dz[k:])
        nlevs = levels_in_tplayer(dz_array, tp_thickness)
        if not nlevs:
            return np.nan, np.nan, np.nan, np.nan
        
        # Return tropopause pressure and temperature if any preliminary tropopause fulfills
        # the thickness criterion, cut dz_array at tp_thickness km
        dz_end = tp_thickness - np.sum(dz_array[:nlevs])
        dz_array = np.append(dz_array[:nlevs], dz_end)
        dtdz_array = np.append(-dtdz_crit, dtdz[k:k+nlevs])
        
        # dtdz array flipped as thickness criterion beginns with lowermost layer going up
        if thickness_criterion(dtdz_array, dz_array, dtdz_crit):
            # temperature at tropopause
            ttp = lin_interp(t_profile[ks[i]-1:ks[i]+1], dtdz[ks[i]-1:ks[i]+1], dtdz_crit)
            # pressure at tropopause
            ptp = lin_interp(p_profile[k-1:k+1], z_profile[k-1:k+1], ztp)
            # height calculated at tropopause

            return ttp, ptp, ztp, thickness_flag
        else:
            thickness_flag += 1
    return np.nan, np.nan, np.nan, thickness_flag
    

def thickness_criterion(dtdz, dz, dtdz_crit):
    """
    Check for the WMO's thickness criterion for tropopauses. dtdz has to stay below dtdz_crit K/km on average between 
    the preliminary tropopause and each of the layers above in tp_thickness [m].
    """

    for i in np.arange(2, np.size(dtdz), 1):
        if np.average(dtdz[:i], weights=dz[:i]) < dtdz_crit:
            return False
    return True
    
def levels_in_tplayer(dz_array, tp_thickness):
    i = 0
    while sum(dz_array[:i]) < tp_thickness:
        i += 1
        if i == dz_array.size:
            return False
    return i-1

def lin_interp(y, x, x_int):
    return y[0] + (y[1] - y[0]) / (x[1] - x[0]) * (x_int - x[0])

def zero_crossings(array):
    """
    Finds the array indices after the zero crossings.
    takes: array/numpy array
    returns: numpy array
    """
    
    # Vector of boolean values (True, False) indicating between which
    # indices the sign of the array values changes
    # sign_switches[0] = True would mean between there is a sign change
    # between the first and the second entry in the array
    with np.errstate(invalid='ignore'):
        sign_switches = ((array[:-1] * array[1:]) <= 0)

    # Set up a vector that indicates the positions of the array after
    # each sing change by appending one False at the beginning of sign_switches
    after_switch = np.append(False, sign_switches)
    
    # get the indices in the array that lie after each sign_switch
    indices_after = np.where(after_switch == True)

    return(indices_after)
